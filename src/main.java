import java.util.Scanner;

public class main {
//Classe principale du programme

    public static void main(String[] args) {
        //Permet de récupérer les saisies données

        Scanner scanner = new Scanner(System.in);

        //Alignement requis pour win -> conditions
        int N = 4;
        //Colonnes et lignes pour le tableau
        int C = 7;
        int L = 6;

        //Mise en place du tableau  ('.' = emplacement vide / 'X' = joueur 1 / 'O' = joueur 2)
        //Tableau à 2 dimensions
        char[][] tabl = new char[C][L];

        //On instance le tableau avec le for pour lui donner l'action si la valeur est null
        for(int x = 0 ; x < C ; x++)
            for(int y = 0 ; y < L ; y++)
                tabl[x][y] = '.';


            //Déclare dans la suite
        int winner = 0;

            //Délimite la conditions qui permet d'arrêter le jeu
        for(int i = 1 ; i <= C*L ; i++){

            //Affichage du tableau
            System.out.println("Tour " + i + ", Début du jeu :");

            for(int loop = 0 ; loop < C+2+2*C ; loop++)System.out.print('-');   //Ici, on mets en place les '-' pour délimiter le tableau
            System.out.println();   //Ici, on l'affiche

            for(int y = 0 ; y < L ; y++){
                System.out.print('|');
                for(int x = 0 ; x < C ; x++){
                    System.out.print(" " + tabl[x][y] + " ");
                }
                System.out.print('|');
                System.out.println();
            }

            for (int loop = 0 ; loop < C+2+2*C ; loop++)System.out.print('-');
            System.out.println(); //Le tableau est créé, on applique le même principe que pour les '-', les " " représente l'espace entre les"cases" du jeu


            //On déclare le visuel du tour -> jeton
            System.out.println("Tour du joueur " + (i%2==1 ? 'X' : 'O') ); //On déclare l'instance du tour, si il est pair il affiche 'X', impair 'O'
            System.out.println("Entrez le numéro de la colonne entre 1 et " + C + "...");
            boolean placement = false;  //Permet de savoir si on a effectué l'action du placement
            int column = -1;
            while(!placement){  //On donne une condition afin "d'obliger" l'action de placement et pas une autre
                column = -1;
                String ligne = scanner.nextLine();

                //Vérifie les conditions des nombres entiers
                try{
                    column = Integer.valueOf(ligne);

                    if (column >= 1 && column <= C){
                        if(tabl[column - 1][0] != '.'){     //La colonne -1 permet de prendre la valeur 0 est d'entrer les nombres de 1 a 7 et pas de 0 a 6
                            System.out.println("Colonne compléte, try again");
                        } else {
                            placement = true;
                        }                                                       //Les deux println renvoient la conditions déclarées
                    } else {
                        System.out.println("Nombre incorrect, try again");
                    }
                }catch(Exception e){System.out.println("Nombre incorrect, try again");}
            }

            //Placement du jeton
            int rang = L-1;
            while(tabl[column - 1][rang] != '.'){     //On place le jeton de sorte à ce qui prenne la valeur au dessus de celle déjà occupé, "-1" permet de le placer au début du tableau
                rang--;
            }
            tabl[column - 1][rang] = (i%2==1 ? 'X' : 'O');  //On replace le jeton en fonction du tour, pair ou impair



            //On mets en place le systéme de victoire
            char symbol = (i%2==1 ? 'X' : 'O');

            int max = 0;
            int x; int y;
            int somme;

            //try vertical y
            x = column-1; y = rang; somme=-1;
            while(y >= 0 && tabl[x][y] == symbol){ y--; somme++;}
            y = rang;
            while(y < L && tabl[x][y] == symbol){ y++; somme++;}
            if(somme > max) max= somme;

        }
    }
}
